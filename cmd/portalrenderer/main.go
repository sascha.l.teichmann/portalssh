package main

import (
	"flag"
	"log"

	"github.com/gdamore/tcell/v2"
	"gitlab.com/sascha.l.teichmann/portalssh/renderer"

	_ "github.com/gdamore/tcell/v2/terminfo/extended"
)

func main() {

	var (
		mapfile = flag.String("map", "map.json", "map to render")
	)
	flag.Parse()

	m, err := renderer.LoadMapFromJSONFile(*mapfile)
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}

	r := newRunner(m)

	s, err := tcell.NewScreen()
	if err != nil {
		log.Fatalf("%+v\n", err)
	}
	if err := s.Init(); err != nil {
		log.Fatalf("%+v\n", err)
	}

	func() {
		defer s.Fini()
		r.run(s)
	}()
}
