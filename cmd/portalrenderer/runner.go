package main

import (
	"fmt"
	"image"
	"math"
	"time"

	"github.com/gdamore/tcell/v2"
	"gitlab.com/sascha.l.teichmann/portalssh/renderer"
	"golang.org/x/image/draw"
)

type runner struct {
	canvas *image.RGBA   // canvas for rune extraction.
	buf    *image.RGBA   // used when canvas too large or small.
	m      *renderer.Map // the map to render.
}

func newRunner(m *renderer.Map) *runner {
	return &runner{
		m: m,
	}
}

func limit(r image.Rectangle) image.Rectangle {
	w := renderer.Clamp(r.Dx(), 640, 1024)
	h := renderer.Clamp(r.Dy(), 480, 768)
	return image.Rect(0, 0, w, h)
}

func (r *runner) blitRunes(s tcell.Screen) {
	rc := renderer.RuneConverter{Img: r.canvas}
	width, height := rc.Img.Rect.Dx(), rc.Img.Rect.Dy()

	i := 0
	for y := 0; y < height; y, i = y+8, i+1 {
		j := 0
		for x := 0; x < width; x, j = x+4, j+1 {
			rc.Extract(x, y)
			st := tcell.StyleDefault.
				Foreground(tcell.NewRGBColor(
					int32(rc.FGColor[0]),
					int32(rc.FGColor[1]),
					int32(rc.FGColor[2]))).
				Background(tcell.NewRGBColor(
					int32(rc.BGColor[0]),
					int32(rc.BGColor[1]),
					int32(rc.BGColor[2])))
			s.SetContent(j, i, rc.CodePoint, nil, st)
		}
	}
}

func (r *runner) render(s tcell.Screen) {

	swidth, sheight := s.Size()

	sdim := image.Rect(0, 0, 4*swidth, 8*sheight)

	if r.canvas == nil || !r.canvas.Bounds().Eq(sdim) {
		r.canvas = image.NewRGBA(sdim)
	}

	bdim := limit(sdim)

	var target *image.RGBA

	if sdim.Eq(bdim) { // direct to canvas?
		r.buf = nil
		target = r.canvas
	} else { // use buffer
		if r.buf == nil || !r.buf.Bounds().Eq(bdim) {
			r.buf = image.NewRGBA(bdim)
		}
		target = r.buf
	}

	r.m.Draw(&graphics{
		canvas: target,
		width:  bdim.Dx(),
		height: bdim.Dy(),
	})

	if r.buf != nil { // rendered to buffer -> scale to screen
		draw.ApproxBiLinear.Scale(r.canvas, sdim, r.buf, bdim, draw.Src, nil)
		//draw.BiLinear.Scale(r.canvas, sdim, r.buf, bdim, draw.Src, nil)
	}

	// convert to runes
	r.blitRunes(s)
}

func writeString(sc tcell.Screen, x, y int, s string, st tcell.Style) {
	for _, r := range s {
		sc.SetContent(x, y, r, nil, st)
		x++
	}
}

const (
	usage      = `Move:WASD/Cursor|Look up/down:PgUp/Down|Jump:Space|Crouch:C|Quit:ESC`
	timeFormat = "15:04:05"
)

func (r *runner) hud(s tcell.Screen) {
	st := tcell.StyleDefault.
		Background(tcell.ColorBlack).
		Foreground(tcell.ColorYellow)
	writeString(s, 0, 0, usage, st)

	w, h := s.Size()

	now := time.Now().Format(timeFormat)
	writeString(s, w-len(now), 0, now, st)

	p := r.m.Player

	pos := fmt.Sprintf(
		"Position: %.1f, %.1f, %.1f Angle/Yaw: %.1f, %.1f",
		p.Where.X,
		p.Where.Y,
		p.Where.Z,
		p.Angle*(180/math.Pi),
		p.Yaw)

	writeString(s, 0, h-1, pos, st)
}

type graphics struct {
	canvas *image.RGBA
	width  int
	height int
}

func (g *graphics) Size() (int, int) {
	return g.width, g.height
}

func (r *runner) run(s tcell.Screen) {

	defStyle := tcell.StyleDefault.
		Background(tcell.ColorBlack).
		Foreground(tcell.ColorWhite)
	s.SetStyle(defStyle)

	s.Clear()

	var (
		wasd    [4]bool
		ground  = false
		falling = true
		moving  = false
		ducking = false
	)

	p := r.m.Player

	done := make(chan struct{})
	ticker := time.NewTicker(time.Second / 8)

	defer func() {
		ticker.Stop()
		close(done)
	}()

	events := make(chan tcell.Event)

	go s.ChannelEvents(events, done)

	for {
		r.render(s)
		r.hud(s)
		s.Show()

		var eyeHeight float64
		if ducking {
			eyeHeight = renderer.DuckHeight
		} else {
			eyeHeight = renderer.EyeHeight
		}

		ground = !falling
		if falling {
			p.Velocity.Z -= 0.05 // add gravity
			nextZ := p.Where.Z + p.Velocity.Z
			if p.Velocity.Z < 0 && nextZ < r.m.Sectors[p.Sector].Floor+eyeHeight {
				// When going down
				// Fix to ground
				p.Where.Z = r.m.Sectors[p.Sector].Floor + eyeHeight
				p.Velocity.Z = 0
				falling = false
				ground = true
			} else if p.Velocity.Z > 0 && nextZ > r.m.Sectors[p.Sector].Ceil {
				// When going up
				// Prevent jumping above ceiling
				p.Velocity.Z = 0
				falling = true
			}

			if falling {
				p.Where.Z += p.Velocity.Z
				moving = true
			}
		}
		// Horizontal collision detection
		if moving {
			px, py := p.Where.X, p.Where.Y
			dx, dy := p.Velocity.X, p.Velocity.Y

			sect := r.m.Sectors[p.Sector]
			vertices := sect.Vertices

			pp := renderer.Vector2d{px, py}
			pd := pp.Add(renderer.Vector2d{dx, dy})

			for s := range vertices[:len(vertices)-1] {
				sn := s + 1
				if renderer.IntersectBox(pp, pd, vertices[s], vertices[sn]) &&
					pd.PointSide(vertices[s], vertices[sn]) < 0 {
					// Check where the hole is.
					var holeLow, holeHigh float64
					if sect.Neighbors[s] < 0 {
						holeLow = 9e9
						holeHigh = -9e9
					} else {
						holeLow = math.Max(sect.Floor, r.m.Sectors[sect.Neighbors[s]].Floor)
						holeHigh = math.Min(sect.Ceil, r.m.Sectors[sect.Neighbors[s]].Ceil)
					}
					// Check whether we are bumping into a wall.
					if holeHigh < p.Where.Z+renderer.HeadMargin ||
						holeLow > p.Where.Z+eyeHeight+renderer.KneeHeight {
						// Bumps into wall. Slide along the wall.
						xd := vertices[sn].X - vertices[s].X
						yd := vertices[sn].Y - vertices[s].Y
						dx = xd * (dx*xd + yd*dy) / (xd*xd + yd*yd)
						dy = yd * (dx*xd + yd*dy) / (xd*xd + yd*yd)
						moving = false
					}
				}
			}
			//log.Println(p.Where.X, p.Where.Y)
			p.Move(dx, dy, r.m)
			falling = true
		}

		select {
		case <-ticker.C:

		case ev := <-events:
			switch ev := ev.(type) {
			case *tcell.EventResize:
				r.render(s)
				s.Sync()
			case *tcell.EventKey:
				switch ev.Key() {
				case tcell.KeyEscape, tcell.KeyCtrlC:
					return
				case tcell.KeyRune:
					switch ev.Rune() {
					case 'w':
						wasd[0] = true
					case 's':
						wasd[1] = true
					case 'a':
						wasd[2] = true
					case 'd':
						wasd[3] = true
					case ' ':
						if ground {
							p.Velocity.Z += 0.5
							falling = true
						}
					case 'c':
						ducking = !ducking
						falling = true
					}
				case tcell.KeyPgUp:
					p.Yaw = renderer.Clamp(p.Yaw-0.5, -5.0, 5.0)
				case tcell.KeyPgDn:
					p.Yaw = renderer.Clamp(p.Yaw+0.5, -5.0, 5.0)
				case tcell.KeyUp:
					wasd[0] = true
				case tcell.KeyDown:
					wasd[1] = true
				case tcell.KeyRight:
					if p.Angle += math.Pi / 180 * 5; p.Angle >= 2*math.Pi {
						p.Angle -= 2 * math.Pi
					}
				case tcell.KeyLeft:
					if p.Angle -= math.Pi / 180 * 5; p.Angle < 0 {
						p.Angle += 2 * math.Pi
					}
				}
			}
		}

		p.Move(0, 0, r.m)

		var move [2]float64

		cs, ss := math.Cos(p.Angle)*0.2, math.Sin(p.Angle)*0.2

		if wasd[0] {
			move[0] += cs
			move[1] += ss
		}
		if wasd[1] {
			move[0] -= cs
			move[1] -= ss
		}
		if wasd[2] {
			move[0] += ss
			move[1] -= cs
		}
		if wasd[3] {
			move[0] -= ss
			move[1] += cs
		}

		pushing := wasd[0] || wasd[1] || wasd[2] || wasd[3]
		var acceleration float64
		if pushing {
			acceleration = 0.4
		} else {
			acceleration = 0.2
		}

		p.Velocity.X = p.Velocity.X*(1-acceleration) + move[0]*acceleration
		p.Velocity.Y = p.Velocity.Y*(1-acceleration) + move[1]*acceleration

		if pushing {
			moving = true
		}

		for i := range wasd {
			wasd[i] = false
		}
	}
}

func (g *graphics) VLine(x, y1, y2, top, middle, bottom int) {

	if x < 0 || x >= g.width {
		return
	}

	y1 = renderer.Clamp(y1, 0, g.height-1)
	y2 = renderer.Clamp(y2, 0, g.height-1)

	if y2 < y1 {
		return
	}

	pix := g.canvas.Pix
	p1 := g.canvas.PixOffset(x, y1)

	if y1 == y2 {
		r0, g0, b0 := uint8(middle>>16), uint8(middle>>8), uint8(middle)
		pix[p1+0] = r0
		pix[p1+1] = g0
		pix[p1+2] = b0
		pix[p1+3] = 0xff
		return
	}

	p2 := g.canvas.PixOffset(x, y2)

	r0, g0, b0 := uint8(top>>16), uint8(top>>8), uint8(top)
	pix[p1+0] = r0
	pix[p1+1] = g0
	pix[p1+2] = b0
	pix[p1+3] = 0xff

	r0, g0, b0 = uint8(middle>>16), uint8(middle>>8), uint8(middle)
	col := []uint8{r0, g0, b0, 0xff}
	stride := g.canvas.Stride
	for p1 += stride; p1 < p2; p1 += stride {
		copy(pix[p1:], col)
	}

	r0, g0, b0 = uint8(bottom>>16), uint8(bottom>>8), uint8(bottom)
	pix[p2+0] = r0
	pix[p2+1] = g0
	pix[p2+2] = b0
	pix[p2+3] = 0xff
}
