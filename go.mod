module gitlab.com/sascha.l.teichmann/portalssh

go 1.18

require (
	github.com/creack/pty v1.1.18
	github.com/gdamore/tcell/v2 v2.4.0
	github.com/gliderlabs/ssh v0.3.3
	golang.org/x/image v0.0.0-20220321031419-a8550c1d254a
)

require (
	github.com/anmitsu/go-shlex v0.0.0-20200514113438-38f4b401e2be // indirect
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	golang.org/x/crypto v0.0.0-20220321153916-2c7772ba3064 // indirect
	golang.org/x/sys v0.0.0-20220328115105-d36c6a25d886 // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
	golang.org/x/text v0.3.7 // indirect
)
