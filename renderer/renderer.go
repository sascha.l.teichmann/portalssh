package renderer

const (
//Width  = 640
//Height = 400
)

const (
	EyeHeight  = 6   // Camera height from floor when standing
	DuckHeight = 2.5 // ... and when crouching.
	HeadMargin = 1   // How much room there is above camera before the head hits the ceiling.
	KneeHeight = 2   // How tall obstacles the player can simply walk w/o jumping.
)

const (
//HFoV = 0.73 * Height // Affects the horizontal field of vision.
//VFoV = 0.20 * Height // Affects the vertical field of vision.
)

type Sector struct {
	Floor     float64
	Ceil      float64
	Vertices  []Vector2d
	Neighbors []int
}

type Map struct {
	Player  *Player
	Sectors []*Sector
}

type Player struct {
	Where    Vector3d // Current position.
	Velocity Vector3d // Current motion vector.

	Angle float64 // Looking towards (and sin/os thereof)
	Yaw   float64

	Sector int // which sector the player is currently in.
}

func (p *Player) Move(dx, dy float64, m *Map) {

	pp := Vector2d{p.Where.X, p.Where.Y}
	pd := pp.Add(Vector2d{dx, dy})
	// Check if this movement crosses one of this sector's edges
	// that have a neighboring sector on the other side.
	// Because the edge vertices of each sector are defined in
	// clockwise order, PointSide will always return -1 for a point
	// that is outside the sector and 0 or 1 for a point that is inside.
	sect := m.Sectors[p.Sector]

	vertices := sect.Vertices
	for s := range vertices[:len(vertices)-1] {
		//sn := (s + 1) % len(vertices)
		sn := s + 1
		if sect.Neighbors[s] >= 0 &&
			IntersectBox(pp, pd, vertices[s], vertices[sn]) &&
			pd.PointSide(vertices[s], vertices[sn]) < 0 {
			p.Sector = sect.Neighbors[s]
			break
		}
	}

	p.Where.X += dx
	p.Where.Y += dy
}

type Graphics interface {
	VLine(x, y1, y2, top, middle, bottom int)
	Size() (int, int)
}

type Queue[T any] []T

func (q *Queue[T]) Push(t T) {
	*q = append(*q, t)
}

func (q Queue[T]) Empty() bool { return len(q) == 0 }

func (q *Queue[T]) Pop() T {
	t := (*q)[0]
	var n T
	(*q)[0] = n
	*q = (*q)[1:]
	return t
}

type item struct {
	sector int
	sx1    int
	sx2    int
}

const (
	nearz    float64 = 1e-4
	farz     float64 = 5
	nearside float64 = 1e-5
	farside  float64 = 20
)

func (m *Map) Draw(gfx Graphics) {

	Width, Height := gfx.Size()

	HFoV := 0.73 * float64(Height) // Affects the horizontal field of vision.
	VFoV := 0.20 * float64(Height) // Affects the vertical field of vision.

	q := Queue[item]{}
	q.Push(item{
		sector: m.Player.Sector,
		sx1:    0,
		sx2:    Width - 1,
	})

	yTop := make([]int, Width)

	yBottom := make([]int, Width)
	for i := range yBottom {
		yBottom[i] = Height - 1
	}

	renderedSectors := make([]int, len(m.Sectors))

	p := m.Player
	//pcos, psin := math.Cos(p.Angle), math.Sin(p.Angle)
	p2d := Vector2d{p.Where.X, p.Where.Y}
	rot := Rotate(p.Angle)

	for !q.Empty() {
		// Pick a sector and slice from the queue.
		now := q.Pop()
		//log.Printf("sector: %d %d %d\n", now.sector, now.sx1, now.sx2)
		// Odd = still rendering
		// 0x20 = give up
		if renderedSectors[now.sector]&0x21 != 0 {
			continue
		}
		renderedSectors[now.sector]++
		sect := m.Sectors[now.sector]

		vertices := sect.Vertices

		// Render each wall of this sector facing the player.
		for s := range vertices[:len(vertices)-1] {
			//log.Printf("** %d\n", s)
			//sn := (s + 1) % len(vertices)
			sn := s + 1
			// Acquire the x,y coordinates of the two endpoints (vertices) of this edge of the sector
			v1 := vertices[s].Sub(p2d)
			v2 := vertices[sn].Sub(p2d)

			// Rotate them around the player's view
			t1 := rot.Mul(v1)
			t2 := rot.Mul(v2)
			// Is the wall at least partially in front of the player?

			if t1.Y <= 0 && t2.Y <= 0 {
				continue
			}
			// If it's partially behind the player
			// clip it against player's view frustrum
			if t1.Y <= 0 || t2.Y <= 0 {
				// Find an intersection between the wall
				// and the approximate edges of player's view

				i1 := Intersection(t1, t2, Vector2d{-nearside, nearz}, Vector2d{-farside, farz})
				i2 := Intersection(t1, t2, Vector2d{nearside, nearz}, Vector2d{farside, farz})

				if t1.Y < nearz {
					if i1.Y > 0 {
						t1 = i1
					} else {
						t1 = i2
					}
				}
				if t2.Y < nearz {
					if i1.Y > 0 {
						t2 = i1
					} else {
						t2 = i2
					}
				}
			}
			// Do perspective transformation
			xscale1 := HFoV / t1.Y
			yscale1 := VFoV / t1.Y
			x1 := Width/2 - int(t1.X*xscale1)

			xscale2 := HFoV / t2.Y
			yscale2 := VFoV / t2.Y
			x2 := Width/2 - int(t2.X*xscale2)
			// Only render if it's visible

			if x1 >= x2 || x2 < now.sx1 || x1 > now.sx2 {
				continue
			}
			// Acquire the floor and ceiling heights,
			// relative to where the player's view is
			yceil := sect.Ceil - p.Where.Z
			yfloor := sect.Floor - p.Where.Z

			// Check the edge type. neighbor=-1 means wall,
			// other=boundary between two sectors.
			neighbor := sect.Neighbors[s]
			var nyceil, nyfloor float64
			if neighbor >= 0 { // Is another sector showing through this portal?
				nyceil = m.Sectors[neighbor].Ceil - p.Where.Z
				nyfloor = m.Sectors[neighbor].Floor - p.Where.Z
			}
			// Project our ceiling & floor heights into screen coordinates (Y coordinate)
			y1a := Height/2 - int(p.projYaw(yceil, t1.Y)*yscale1)
			y1b := Height/2 - int(p.projYaw(yfloor, t1.Y)*yscale1)
			y2a := Height/2 - int(p.projYaw(yceil, t2.Y)*yscale2)
			y2b := Height/2 - int(p.projYaw(yfloor, t2.Y)*yscale2)
			//The same for the neighboring sector
			ny1a := Height/2 - int(p.projYaw(nyceil, t1.Y)*yscale1)
			ny1b := Height/2 - int(p.projYaw(nyfloor, t1.Y)*yscale1)
			ny2a := Height/2 - int(p.projYaw(nyceil, t2.Y)*yscale2)
			ny2b := Height/2 - int(p.projYaw(nyfloor, t2.Y)*yscale2)

			// Render the wall.
			beginx, endx := Max(x1, now.sx1), Min(x2, now.sx2)
			//log.Printf("  beginx: %d endx: %d\n", beginx, endx)

			for x := beginx; x <= endx; x++ {
				// Calculate the Z coordinate for this point. (Only used for lighting.)
				z := int((float64(x-x1)*(t2.Y-t1.Y)/float64(x2-x1) + t1.Y) * 8)
				// Acquire the Y coordinates for our ceiling & floor for this X coordinate. Clamp them.
				ya := (x-x1)*(y2a-y1a)/(x2-x1) + y1a
				cya := Clamp(ya, yTop[x], yBottom[x]) // top
				yb := (x-x1)*(y2b-y1b)/(x2-x1) + y1b
				cyb := Clamp(yb, yTop[x], yBottom[x]) // bottom

				// Render ceiling: everything above this sector's ceiling height.
				gfx.VLine(x, yTop[x], cya-1, 0x11_11_11, 0x22_22_22, 0x11_11_11)
				// Render floor: everything below this sector's floor height.
				gfx.VLine(x, cyb+1, yBottom[x], 0x00_00_ff, 0x00_00_aa, 0x00_00_ff)

				// Is there another sector behind this edge?
				if neighbor >= 0 {
					// Same for _their_ floor and ceiling
					nya := (x-x1)*(ny2a-ny1a)/(x2-x1) + ny1a
					cnya := Clamp(nya, yTop[x], yBottom[x])
					nyb := (x-x1)*(ny2b-ny1b)/(x2-x1) + ny1b
					cnyb := Clamp(nyb, yTop[x], yBottom[x])
					// If our ceiling is higher than their ceiling, render upper wall
					r1 := 0x010101 * (255 - z)
					r2 := 0x040007 * (31 - z/8)
					var c int
					if !(x == x1 || x == x2) {
						c = r1
					}
					gfx.VLine(x, cya, cnya-1, 0, c, 0) // Between our and their ceiling
					yTop[x] = Clamp(Max(cya, cnya), yTop[x], Height-1)
					// Shrink the remaining window below these ceilings
					// If our floor is lower than their floor, render bottom wall */
					if x == x1 || x == x2 {
						c = 0
					} else {
						c = r2
					}
					gfx.VLine(x, cnyb+1, cyb, 0, c, 0) // Between their and our floor
					yBottom[x] = Clamp(Min(cyb, cnyb), 0, yBottom[x])
					// Shrink the remaining window above these floors
				} else {
					// There's no neighbor.
					// Render wall from top (cya = ceiling level) to bottom (cyb = floor level).
					r := 0x010101 * (255 - z)
					var c int
					if !(x == x1 || x == x2) {
						c = r
					}
					gfx.VLine(x, cya, cyb, 0, c, 0)
				}
			}
			// Schedule the neighboring sector for rendering within the window formed by this wall.
			//log.Printf("neighbor: %d\n", neighbor)
			if neighbor >= 0 && endx >= beginx {
				//log.Printf("  -> beginx: %d endx: %d\n", beginx, endx)
				q.Push(item{
					sector: neighbor,
					sx1:    beginx,
					sx2:    endx,
				})
			}
		} // for s in sector's edges
		renderedSectors[now.sector]++
	}
}

func (p *Player) projYaw(y, z float64) float64 {
	return y + z*p.Yaw
}
