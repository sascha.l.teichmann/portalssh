package renderer

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"math"
	"os"
)

type vertex struct {
	Y  float64   `json:"y"`
	Xs []float64 `json:"xs"`
}

type sector struct {
	Floor     float64 `json:"floor"`
	Ceil      float64 `json:"ceil"`
	Vertices  []int   `json:"vi"`
	Neighbors []int   `json:"ni"`
}

type player struct {
	X      float64 `json:"x"`
	Y      float64 `json:"y"`
	Angle  float64 `json:"angle"`
	Sector int     `json:"sector"`
}

type jsonMap struct {
	Vertices []vertex `json:"vertices"`
	Sectors  []sector `json:"sectors"`
	Player   player   `json:"player"`
}

func LoadMapFromJSONFile(fname string) (*Map, error) {
	f, err := os.Open(fname)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	return LoadMapJSON(f)
}

func LoadMapJSON(r io.Reader) (*Map, error) {
	var jm jsonMap
	dec := json.NewDecoder(r)
	if err := dec.Decode(&jm); err != nil {
		return nil, err
	}
	return jm.compile()
}

func (jm *jsonMap) compile() (*Map, error) {
	verts, err := jm.compileVertices()
	if err != nil {
		return nil, err
	}
	if len(verts) == 0 {
		return nil, errors.New("no vertices")
	}
	log.Printf("num vertices: %d\n", len(verts))
	secs, err := jm.compileSectors(verts)
	if err != nil {
		return nil, err
	}
	pl, err := jm.compilePlayer(secs)
	if err != nil {
		return nil, err
	}
	return &Map{
		Sectors: secs,
		Player:  pl,
	}, nil
}

func (jm *jsonMap) compileVertices() ([]Vector2d, error) {
	var verts []Vector2d
	for i := range jm.Vertices {
		v := &jm.Vertices[i]
		for _, x := range v.Xs {
			verts = append(verts, Vector2d{X: x, Y: v.Y})
		}
	}
	return verts, nil
}

func (jm *jsonMap) compileSectors(verts []Vector2d) ([]*Sector, error) {
	if len(jm.Sectors) == 0 {
		return nil, errors.New("no sectors")
	}
	secs := make([]*Sector, 0, len(jm.Sectors))

	for i := range jm.Sectors {
		s := &jm.Sectors[i]
		if len(s.Vertices) == 0 {
			return nil, fmt.Errorf("no vertices in Sector %d", i)
		}
		if len(s.Vertices) != len(s.Neighbors) {
			return nil, fmt.Errorf(
				"number of vertices an neighbors mismatch in sector %d", i)
		}
		vs := make([]Vector2d, len(s.Vertices)+1)
		for j, vIdx := range s.Vertices {
			if vIdx < 0 || vIdx >= len(verts) {
				return nil, fmt.Errorf("invalid vertex index %d in sector %d", vIdx, i)
			}
			vs[j+1] = verts[vIdx]
		}
		// Ensure the vertices form a ring.
		vs[0] = vs[len(vs)-1]
		secs = append(secs, &Sector{
			Floor:     s.Floor,
			Ceil:      s.Ceil,
			Vertices:  vs,
			Neighbors: s.Neighbors,
		})
	}

	// check neighbor indices
	for i, s := range secs {
		for _, n := range s.Neighbors {
			if n >= 0 && n >= len(secs) {
				return nil, fmt.Errorf("invalid neighbor %d in sector %d", n, i)
			}
		}
	}

	return secs, nil
}

func (jm *jsonMap) compilePlayer(secs []*Sector) (*Player, error) {
	if jm.Player.Sector < 0 || jm.Player.Sector >= len(secs) {
		return nil, errors.New("player sector out of range")
	}
	ang := jm.Player.Angle
	for ; ang < 0; ang += math.Pi * 2 {
	}
	for ; ang >= math.Pi*2; ang -= math.Pi * 2 {
	}
	return &Player{
		Where: Vector3d{
			X: jm.Player.X,
			Y: jm.Player.Y,
			Z: secs[jm.Player.Sector].Floor + EyeHeight,
		},
	}, nil
}
