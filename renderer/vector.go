package renderer

import "math"

// Vector2d has x and y coordinate.
type Vector2d struct {
	X float64
	Y float64
}

type Vector3d struct {
	X float64
	Y float64
	Z float64
}

type Matrix2d struct {
	M00, M01,
	M10, M11 float64
}

type number interface {
	int | float64
}

func Min[T number](a, b T) T {
	if a < b {
		return a
	}
	return b
}

func Max[T number](a, b T) T {
	if a > b {
		return a
	}
	return b
}

func Clamp[T number](a, mi, ma T) T {
	return Min[T](Max[T](a, mi), ma)
}

// Overlap determines wether the number ranges Overlap.
func Overlap(a0, a1, b0, b1 float64) bool {
	return math.Min(a0, a1) <= math.Max(b0, b1) &&
		math.Min(b0, b1) <= math.Max(a0, a1)
}

func Rotate(alpha float64) Matrix2d {
	sin, cos := math.Sin(alpha), math.Cos(alpha)
	return Matrix2d{
		M00: sin, M01: -cos,
		M10: cos, M11: sin,
	}
}

func (m Matrix2d) Mul(v Vector2d) Vector2d {
	return Vector2d{
		m.M00*v.X + m.M01*v.Y,
		m.M10*v.X + m.M11*v.Y,
	}
}

func (v Vector2d) Sub(w Vector2d) Vector2d {
	return Vector2d{X: v.X - w.X, Y: v.Y - w.Y}
}

func (v Vector2d) Add(w Vector2d) Vector2d {
	return Vector2d{X: v.X + w.X, Y: v.Y + w.Y}
}

func (v Vector2d) Scale(s float64) Vector2d {
	return Vector2d{X: s * v.X, Y: s * v.Y}
}

// Cross is the vector cross product.
func (v Vector2d) Cross(w Vector2d) float64 {
	return v.X*w.Y - w.X*v.Y
}

// PointSide determines which side of a line the point is on.
// return value: < 0, = 0 or
func (v Vector2d) PointSide(a, b Vector2d) float64 {
	return b.Sub(a).Cross(v.Sub(a))
}

// Intersect calculates the point of intersection between two lines.
func Intersection(u1, u2, u3, u4 Vector2d) Vector2d {
	l := u1.Sub(u2).Cross(u3.Sub(u4))
	v0 := u1.Cross(u2)
	v1 := u3.Cross(u4)
	return Vector2d{
		X: Vector2d{v0, u1.X - u2.X}.Cross(Vector2d{v1, u3.X - u4.X}),
		Y: Vector2d{v0, u1.Y - u2.Y}.Cross(Vector2d{v1, u3.Y - u4.Y}),
	}.Scale(1 / l)
}

// IntersectBox determines wether 2D boxes intersect.
func IntersectBox(u1, u2, u3, u4 Vector2d) bool {
	return Overlap(u1.X, u2.X, u3.X, u4.X) &&
		Overlap(u1.Y, u2.Y, u3.Y, u4.Y)
}
